import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";

import ToDos from './components/Todos/Todos';
import MovieApp from './components/Movies/MovieApp';


class App extends Component {
  render() {
    return (
        <Switch>
          <Route path="/" exact component={ToDos} />
          <Route path="/moviesapp" component={MovieApp} />
        </Switch>
    );
  }
}

export default App;
