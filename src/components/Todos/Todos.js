import React, { Component, Fragment } from 'react';
import Header from '../../shared/Header/Header';
import AddTaskForm from './addTaskForm/AddTaskForm';
import Task from './task/Task';
import axios from 'axios';
import Spinner from '../UI/Spinner/Spinner';

class ToDos extends Component {

    state = {
        tasks: [],
        isLoading: false
    };

    loadTasksHandler = () => {
        this.setState({isLoading: true});

        axios.get('/tasks.json').then((response) => {

         const tasks = [];

         for (let key in response.data) {
             tasks.push({...response.data[key], id: key});
         }

         this.setState({tasks, isLoading: false});

       });
    };

    taskRemoveHandler = id => {
        axios.delete(`/tasks/${id}.json`).then(() => {
            this.setState(prevState => {
                const tasks = [...prevState.tasks];
                const index = tasks.findIndex(task => task.id === id);
                tasks.splice(index, 1);
                return {tasks};
            })
        })
    };

    render() {

        const tasks = (
            <div className="col-md-8">
                {
                    this.state.tasks.map((task) => {
                        if(this.state.tasks !== 0) {
                            return <Task text={task.text} key={task.id} remove={() => this.taskRemoveHandler(task.id)}/>
                        } else {
                            return false
                        }
                    })
                }
            </div>
        );

        const isLoading = this.state.isLoading;

        return (
            <Fragment>
                <Header />
                <div className="container">
                    <AddTaskForm />
                    <div className="row justify-content-md-center">
                        <div className="col-md-12">
                            <button className="btn btn-primary btn-block btn-load-tasks" type="button" onClick={this.loadTasksHandler}>Load tasks</button>
                        </div>
                        {tasks}
                    </div>
                    {isLoading && <div className="row justify-content-md-center">
                        <div className="col-md-12">
                            <Spinner />
                        </div>
                    </div>}
                </div>
            </Fragment>
        );
    }
}

export default ToDos;