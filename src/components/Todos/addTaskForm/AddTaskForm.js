import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Spinner from '../../UI/Spinner/Spinner';


class AddTaskForm extends Component {

  state = {
    task: {
      text: ''
    },
    isLoading: false
  };


  taskCreateHandler = () => {
    this.setState({isLoading: true});

    axios.post('/tasks.json',  this.state.task).then(() => {
      this.setState({isLoading: false});
    })
  };

  taskTextChanged = event => {
    event.persist();

    this.setState(prevState => {
      return {
        task: {...prevState.task, [event.target.name]: event.target.value}
      }
    });
  };

  render() {
    const isLoading = this.state.isLoading;

    return (
      <Fragment>
        <div className='row justify-content-md-center items-row'>
          <div className='col-md-6'>
            <div className="input-group">
              <input type="text" className="form-control" name="text" value={this.state.task.text} onChange={this.taskTextChanged} />
              <span className="input-group-btn">
                <button className="btn btn-success" type="button" onClick={this.taskCreateHandler}>Add task</button>
              </span>
            </div>
          </div>
        </div>
        {isLoading && <div className="row justify-content-md-center">
          <div className="col-md-6">
            <Spinner />
          </div>
        </div>}
      </Fragment>
    )
  }
}

export default AddTaskForm;