import React from 'react';

const Task = (props) => {
  return (
    <div className="row justify-content-between alert">
      <div className="col-md-9">
        <p>{props.text}</p>
      </div>
      <div className="col-md-3">
        <button type="button" className="btn btn-warning" onClick={props.remove}>Delete</button>
      </div>
    </div>
  )
};

export default Task;