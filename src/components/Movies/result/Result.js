import React, {Component} from 'react';
import Movie from "./movie/Movie";

class Result extends Component {

   render() {
     const moviesArray = this.props.data.map((movie, index) =>{
       return (
         <div>
           <Movie  nn={index + 1}
                   id={movie.id}
                   name={movie.name}
                   clickHandler={this.props.removeHandler}
                   edit={this.props.edit}
           />
         </div>
       )
     });

    return (
       <div className="result-block">
         <div className="result-block__items">
           To watch later:
             <button className="btn btn-primary btn-block btn-load-tasks" type="button" onClick={this.props.loadMovies}>Load movies</button>
          {moviesArray}
         </div>
       </div>
    )
  }
};

export default Result;