import React,  {Component, Fragment} from 'react';
import axios from 'axios';
import Spinner from '../../UI/Spinner/Spinner';

class Form extends Component {
    state = {
        movie: {
            name: ''
        },
        isLoading: false
    };

    movieCreateHandler = () => {
        this.setState({isLoading: true});

        axios.post('/movies.json',  this.state.movie).then(() => {
            this.setState({isLoading: false});
        })
    };

    movieNameChanged = event => {
        event.persist();

        this.setState(prevState => {
            return {
                movie: {...prevState.movie, [event.target.name]: event.target.value}
            }
        });
    };

  render() {

      const isLoading = this.state.isLoading;

      return (
          <div className="form-block">
              <div className="row">
                  <div className="col-md-10">
                      <div className="input-group">
                          <input type="text" className="form-control" placeholder="Item name" name="name" value={this.state.movie.name} onChange={this.movieNameChanged} />
                      </div>
                  </div>
                  <div className="col-md-2">
                      <button type="button" className="btn btn-primary btn-block" onClick={this.movieCreateHandler}>Add</button>
                  </div>
              </div>
              {isLoading && <div className="row justify-content-md-center">
                  <div className="col-md-6">
                      <Spinner />
                  </div>
              </div>}
          </div>
      )
  }
};

export default Form;