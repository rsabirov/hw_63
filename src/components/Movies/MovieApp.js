import React, {Component, Fragment} from 'react';
import Form from './form-block/Form';
import Result from './result/Result';
import Header from '../../shared/Header/Header';
import axios from 'axios';
import Spinner from '../UI/Spinner/Spinner';

class MovieApp extends Component {

    state = {
        movies: [],
        isLoading: false
    };

    loadMoviesHandler = () => {
        this.setState({isLoading: true});

        axios.get('/movies.json').then((response) => {

            const movies = [];

            for (let key in response.data) {
                movies.push({...response.data[key], id: key});
            }

            this.setState({movies, isLoading: false});

        });
    };

    movieRemoveHandler = id => {
        axios.delete(`/movies/${id}.json`).then(() => {
            this.setState(prevState => {
                const movies = [...prevState.movies];
                const index = movies.findIndex(movie => movie.id === id);
                movies.splice(index, 1);
                return {movies};
            })
        })
    };

    render() {

        const isLoading = this.state.isLoading;

        return (
            <Fragment>
                <Header />
                <div className="container">
                    <Form />
                    {isLoading && <div className="row justify-content-md-center">
                        <div className="col-md-12">
                            <Spinner />
                        </div>
                    </div>}
                    <Result data={this.state.movies}
                            removeHandler={() => this.movieRemoveHandler()}
                            loadMovies={() => this.loadMoviesHandler()}
                    />
                </div>
            </Fragment>
        );
    }
}

export default MovieApp;
